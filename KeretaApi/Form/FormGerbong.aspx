﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FormGerbong.aspx.vb" Inherits="KeretaApi.FormGerbong" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 67%;
        height: 124px;
    }
    .style2
    {
        width: 167px;
    }
    .style3
    {
        width: 25px;
    }
    .style4
    {
        font-family: "Monotype Corsiva";
        font-size: x-large;
        width: 233px;
        color: #000000;
    }
    .style5
    {
        width: 25px;
        font-family: "Monotype Corsiva";
        font-size: x-large;
    }
    .style6
    {
        width: 233px;
    }
    .style7
    {
        width: 591px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
    <tr>
        <td class="style2">
            &nbsp;</td>
        <td class="style5">
            &nbsp;</td>
        <td class="style4">
            <strong>DATA GERBONG</strong></td>
        <td class="style7">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            ID Gerbong</td>
        <td class="style3">
            :</td>
        <td class="style6">
    <asp:TextBox ID="txt_idg" runat="server" Width="154px"></asp:TextBox>
        </td>
        <td class="style7">
        <asp:TextBox ID="txt_state" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style2">
            ID Petugas</td>
        <td class="style3">
            :</td>
        <td class="style6">
    <asp:DropDownList ID="ddlnamap" runat="server" Height="16px" Width="156px">
    </asp:DropDownList>
        </td>
        <td class="style7">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            Nama Gerbong</td>
        <td class="style3">
            :</td>
        <td class="style6">
        <asp:DropDownList ID="ddl_nama" runat="server" Height="30px" Width="155px">
            <asp:ListItem>GR</asp:ListItem>
            <asp:ListItem>GWRU</asp:ListItem>
            <asp:ListItem>DGGW</asp:ListItem>
        </asp:DropDownList>
        </td>
        <td class="style7">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            Posisi Duduk</td>
        <td class="style3">
            :</td>
        <td class="style6">
            <asp:TextBox ID="txt_posduk" 
        runat="server" Width="154px" Height="22px"></asp:TextBox>
        </td>
        <td class="style7">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style2">
            Jumlah Kursi</td>
        <td class="style3">
            :</td>
        <td class="style6">
            <asp:TextBox ID="txt_kursi" 
        runat="server" Width="155px" Height="22px"></asp:TextBox>
        </td>
        <td class="style7">
            &nbsp;</td>
    </tr>
</table>
<p>
        <asp:Button ID="btn_baru" runat="server" Text="Baru" style="height: 26px" />
        <asp:Button ID="btn_simpan" runat="server" Text="Simpan" />
        <asp:Button ID="btn_batal" runat="server" Text="Batal" />
        <asp:Button ID="btn_hapus" runat="server" Text="Hapus" />
        <asp:Button ID="btn_tutup" runat="server" Text="Tutup" />
    </p>
<p>
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
    </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            PageSize="5" Width="590px" BackColor="White" BorderColor="#CC9966" 
            BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" />
                <asp:BoundField DataField="id_gerbong" HeaderText="ID GERBONG" />
                <asp:BoundField DataField="id_petugas" HeaderText="ID PETUGAS" />
                <asp:BoundField DataField="nama_gerbong" HeaderText="NAMA GERBONG" />
                <asp:BoundField DataField="posisi_duduk" HeaderText="POSISI DUDUK" />
                <asp:BoundField DataField="jumlah_kursi" HeaderText="JUMLAH KURSI" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
    </asp:Content>
