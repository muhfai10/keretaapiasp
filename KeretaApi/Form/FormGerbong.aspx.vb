﻿Public Class FormGerbong
    Inherits System.Web.UI.Page
    Private _sql_scalar As String

    Private Property sql_scalar(ByVal p1 As Object, ByVal p2 As String, ByVal p3 As Boolean) As String
        Get
            Return _sql_scalar
        End Get
        Set(ByVal value As String)
            _sql_scalar = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            load_gridview()
            object_state(True)
            button_state(0)
        End If
    End Sub

    Sub object_clear()
        txt_state.Text = ""
        txt_idg.Text = ""
        txt_posduk.Text = ""
        txt_kursi.Text = ""
        Label1.Text = ""
    End Sub

    Sub object_fill(ByVal row As GridViewRow)
        ddlnamap.Items.Clear()
        txt_idg.Text = row.Cells(1).Text
        ddlnamap.Items.Add(row.Cells(2).Text)
        ddl_nama.Text = row.Cells(3).Text
        txt_posduk.Text = row.Cells(4).Text
        txt_kursi.Text = row.Cells(5).Text
    End Sub

    Sub button_state(ByVal id As Integer)
        Select Case id
            Case 0 'default
                btn_baru.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = False
                btn_hapus.Enabled = False
                btn_simpan.Enabled = False
            Case 1 'new
                btn_simpan.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_baru.Enabled = False
                btn_hapus.Enabled = False
            Case 2 'edit
                btn_baru.Enabled = False
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_hapus.Enabled = True
                btn_simpan.Enabled = True
        End Select

    End Sub

    Sub object_state(ByVal kunci As Boolean)
        txt_idg.ReadOnly = Not kunci
        ddlnamap.Enabled = True
        ddl_nama.Enabled = True
        txt_posduk.ReadOnly = Not kunci
        txt_kursi.ReadOnly = Not kunci
    End Sub

    Protected Sub btn_baru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_baru.Click
        Try
            If sql_conn(Session("currCon")) = True Then
                Dim sql1 As String = "select id_petugas from Petugas"
                Dim RS1 As OleDb.OleDbDataReader
                RS1 = sql_datareader(Session("currCon"), sql1)
                ddlnamap.Items.Clear()
                If RS1.HasRows Then
                    While RS1.Read
                        ddlnamap.Items.Add(RS1("id_petugas"))
                    End While
                End If
            End If
            object_clear()
            object_state(True)
            button_state(1)
            txt_state.Text = "new"
            Page.SetFocus(txt_idg)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btn_simpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_simpan.Click
        Try
            If sql_conn(Session("currCon")) = True Then
                Dim sql As String = ""
                If txt_state.Text = "new" Then
                    sql = "insert into Gerbong values ('{0}','{1}','{2}','{3}','{4}')"
                    sql = String.Format(sql, txt_idg.Text, ddlnamap.Text, ddl_nama.Text, txt_posduk.Text, txt_kursi.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_idg.Text = "id_gerbong" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Disimpan"
                    End If


                ElseIf txt_state.Text = "edit" Then
                    sql = "update Gerbong set id_gerbong='{0}', id_petugas='{1}', nama_gerbong='{2}', posisi_duduk='{3}', jumlah_kursi='{4}' where id_gerbong='{0}'"
                    sql = String.Format(sql, txt_idg.Text, ddlnamap.Text, ddl_nama.Text, txt_posduk.Text, txt_kursi.Text, txt_idg.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_idg.Text = "id_gerbong" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Diubah"
                    End If
                End If
            End If
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
        Catch ex As Exception
            MsgBox(Err.Description, vbOKOnly, "Error")
        End Try
    End Sub

    Sub load_gridview()
        If sql_conn(Session("currCon")) = True Then
            Dim tbl_grid As New DataTable
            tbl_grid = sql_datatable(Session("currCon"), "SELECT * FROM gerbong ORDER BY id_gerbong")
            GridView1.DataSource = tbl_grid
            GridView1.DataBind()
            sql_conn_close(Session("currCon"))
        End If
    End Sub

    Private Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        load_gridview()
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    Private Sub GridView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        Dim row_edit As GridViewRow = GridView1.Rows(e.NewSelectedIndex)
        object_clear()
        object_state(True)
        button_state(2)
        object_fill(row_edit)
        txt_state.Text = "edit"
    End Sub

    Protected Sub btn_batal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_batal.Click
        object_clear()
        object_state(True)
        button_state(0)
    End Sub

    Protected Sub btn_hapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_hapus.Click
        If sql_conn(Session("currCon")) = True Then
            sql_execute(Session("currCon"), "DELETE FROM Gerbong WHERE id_gerbong='" & txt_idg.Text & "'")
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
            Label1.Text = "Data berhasil dihapus"
        End If
    End Sub

    Protected Sub btn_tutup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_tutup.Click
        sql_conn_close(Session("currCon"))
        Response.Redirect("~/default.aspx")
    End Sub


 


End Class