﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FormJadwal.aspx.vb" Inherits="KeretaApi.FormJadwal" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 78%;
        height: 99px;
    }
    .style4
    {
        width: 631px;
    }
    .style5
    {
        width: 33px;
    }
    .style6
    {
        width: 33px;
        height: 38px;
    }
    .style8
    {
        width: 631px;
        height: 38px;
    }
    .style11
    {
        width: 224px;
    }
    .style12
    {
        width: 224px;
        height: 38px;
    }
    .style13
    {
        width: 191px;
    }
    .style14
    {
        width: 191px;
        height: 38px;
    }
    .style15
    {
        width: 191px;
        font-family: "Monotype Corsiva";
        font-size: x-large;
        color: #000000;
    }
    .style16
    {
        width: 631px;
        font-size: x-large;
    }
    .style17
    {
        font-size: x-large;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
    <tr>
        <td class="style11">
            <span class="style17">
        </td>
        <td class="style5">
            </span>
        </td>
        <td class="style15">
            <strong>DATA JADWAL</strong></td>
        <td class="style16">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style11">
            ID Jadwal</td>
        <td class="style5">
            :</td>
        <td class="style13">
        <asp:TextBox ID="txt_id" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style4">
        <asp:TextBox ID="txt_state" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style12">
            Waktu Keberangkatan</td>
        <td class="style6">
            :</td>
        <td class="style14">
        <asp:TextBox ID="txt_id0" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style8">
        </td>
    </tr>
    <tr>
        <td class="style11">
            Waktu Kedatangan</td>
        <td class="style5">
            :</td>
        <td class="style13">
        <asp:TextBox ID="txt_id1" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style4">
            <br />
        </td>
    </tr>
</table>
<p>
        <asp:Button ID="btn_baru" runat="server" Text="Baru" style="height: 26px" />
        <asp:Button ID="btn_simpan" runat="server" Text="Simpan" />
        <asp:Button ID="btn_batal" runat="server" Text="Batal" />
        <asp:Button ID="btn_hapus" runat="server" Text="Hapus" />
        <asp:Button ID="btn_tutup" runat="server" Text="Tutup" />
    </p>
    <p>
        <asp:Label ID="Label1" runat="server" Text="Label" Enabled="False"></asp:Label>
    </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            PageSize="5" Width="686px" BackColor="White" BorderColor="#CC9966" 
            BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" />
                <asp:BoundField DataField="id_jadwal" HeaderText="ID JADWAL" />
                <asp:BoundField DataField="waktu_berangkat" HeaderText="WAKTU BERANGKAT" DataFormatString = {0:t} />
                <asp:BoundField DataField="waktu_kedatangan" HeaderText="WAKTU KEDATANGAN" DataFormatString = {0:t} />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
</asp:Content>
