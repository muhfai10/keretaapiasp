﻿Public Class FormJadwal
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            load_gridview()
            object_state(True)
            button_state(0)
        End If
    End Sub
    Sub object_clear()
        txt_state.Text = ""
        txt_id.Text = ""
        txt_id0.Text = ""
        txt_id1.Text = ""
        Label1.Text = ""
    End Sub

    Sub object_fill(ByVal row As GridViewRow)
        txt_id.Text = row.Cells(1).Text
        txt_id0.Text = row.Cells(2).Text
        txt_id1.Text = row.Cells(3).Text
    End Sub

    Sub button_state(ByVal id As Integer)
        Select Case id
            Case 0 'default
                btn_baru.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = False
                btn_hapus.Enabled = False
                btn_simpan.Enabled = False
            Case 1 'new
                btn_simpan.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_baru.Enabled = False
                btn_hapus.Enabled = False
            Case 2 'edit
                btn_baru.Enabled = False
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_hapus.Enabled = True
                btn_simpan.Enabled = True
        End Select
    End Sub

    Sub object_state(ByVal kunci As Boolean)
        txt_id.ReadOnly = Not kunci
        txt_id0.ReadOnly = Not kunci
        txt_id1.ReadOnly = Not kunci
    End Sub

    Protected Sub btn_baru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_baru.Click
        object_clear()
        object_state(True)
        button_state(1)
        txt_state.Text = "new"
        Page.SetFocus(txt_id)
    End Sub
    Protected Sub btn_simpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_simpan.Click
        Try
            If sql_conn(Session("currCon")) = True Then
                Dim sql As String = ""
                If txt_state.Text = "new" Then
                    sql = "insert into Jadwal values ('{0}','{1}','{2}')"
                    sql = String.Format(sql, txt_id.Text, txt_id0.Text, txt_id1.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_id.Text = "id_jadwal" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Disimpan"
                    End If


                ElseIf txt_state.Text = "edit" Then
                    sql = "update Jadwal set id_jadwal='{0}', waktu_berangkat='{1}', waktu_kedatangan='{2}' where id_jadwal='{0}'"
                    sql = String.Format(sql, txt_id.Text, txt_id0.Text, txt_id1.Text, txt_id.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_id.Text = "id_jadwal" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Diubah"
                    End If
                End If
            End If
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
        Catch ex As Exception
            MsgBox(Err.Description, vbOKOnly, "Error")
        End Try
    End Sub

    Sub load_gridview()
        If sql_conn(Session("currCon")) = True Then
            Dim tbl_grid As New DataTable
            tbl_grid = sql_datatable(Session("currCon"), "SELECT * FROM Jadwal ORDER BY id_jadwal")
            GridView1.DataSource = tbl_grid
            GridView1.DataBind()
            sql_conn_close(Session("currCon"))
        End If
    End Sub

    Private Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        load_gridview()
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    Private Sub GridView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        Dim row_edit As GridViewRow = GridView1.Rows(e.NewSelectedIndex)
        object_clear()
        object_state(True)
        button_state(2)
        object_fill(row_edit)
        txt_state.Text = "edit"
    End Sub



    Protected Sub btn_batal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_batal.Click
        object_clear()
        object_state(True)
        button_state(0)
    End Sub

    Protected Sub btn_hapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_hapus.Click
        If sql_conn(Session("currCon")) = True Then
            sql_execute(Session("currCon"), "DELETE FROM Jadwal WHERE id_jadwal='" & txt_id.Text & "'")
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
            Label1.Text = "Data berhasil dihapus"
        End If
    End Sub

    Protected Sub btn_tutup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_tutup.Click
        sql_conn_close(Session("currCon"))
        Response.Redirect("~/default.aspx")
    End Sub
End Class