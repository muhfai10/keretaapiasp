﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FormPelanggan.aspx.vb" Inherits="KeretaApi.FormPelanggan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 72%;
        height: 170px;
    }
    .style2
    {
        width: 104px;
    }
    .style3
    {
        width: 12px;
    }
    .style4
    {
        width: 104px;
        height: 25px;
    }
    .style5
    {
        width: 12px;
        height: 25px;
    }
    .style7
    {
            width: 235px;
        }
    .style8
    {
        height: 25px;
        width: 235px;
    }
    .style9
    {
        width: 235px;
        font-family: "Monotype Corsiva";
        font-size: x-large;
        color: #000000;
    }
    .style10
    {
        width: 267px;
    }
    .style11
    {
        height: 25px;
        width: 267px;
    }
    .style12
    {
        width: 104px;
        height: 26px;
    }
    .style13
    {
        width: 12px;
        height: 26px;
    }
    .style14
    {
        width: 235px;
        height: 26px;
    }
    .style15
    {
        width: 267px;
        height: 26px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <table class="style1">
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
                <td class="style9">
                    <strong>DATA PELANGGAN</strong></td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    ID Pelanggan</td>
                <td class="style5">
                    :</td>
                <td class="style8">
        <asp:TextBox ID="txt_id" runat="server" Width="221px"></asp:TextBox>
                </td>
                <td class="style11">
        <asp:TextBox ID="txt_state" runat="server" Width="167px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Nama Pelanggan</td>
                <td class="style3">
                    :</td>
                <td class="style7">
        <asp:TextBox ID="txt_nama" runat="server" Height="19px" Width="220px"></asp:TextBox>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Alamat</td>
                <td class="style3">
                    :</td>
                <td class="style7">
        <asp:TextBox ID="txt_alamat" runat="server" style="margin-left: 0px" 
            Width="219px"></asp:TextBox>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style12">
                    Tanggal Lahir</td>
                <td class="style13">
                    :</td>
                <td class="style14">
        <asp:TextBox ID="txt_tgl" runat="server" Width="218px"></asp:TextBox>
                </td>
                <td class="style15">
                </td>
            </tr>
            <tr>
                <td class="style2">
                    Jenis Kelamin</td>
                <td class="style3">
                    :</td>
                <td class="style7">
        <asp:DropDownList ID="ddl_jk" runat="server" Height="42px" Width="218px">
            <asp:ListItem>Laki-Laki</asp:ListItem>
            <asp:ListItem>Perempuan</asp:ListItem>
        </asp:DropDownList>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    No. Telepon</td>
                <td class="style3">
                    :</td>
                <td class="style7">
        <asp:TextBox ID="txt_notelp" runat="server" Width="216px"></asp:TextBox>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    Email</td>
                <td class="style3">
                    :</td>
                <td class="style7">
        <asp:TextBox ID="txt_email" runat="server" Width="214px"></asp:TextBox>
                </td>
                <td class="style10">
                    &nbsp;</td>
            </tr>
        </table>
    </p>
    <p>
        <asp:Button ID="btn_baru" runat="server" Text="Baru" style="height: 26px" />
        <asp:Button ID="btn_simpan" runat="server" Text="Simpan" />
        <asp:Button ID="btn_batal" runat="server" Text="Batal" />
        <asp:Button ID="btn_hapus" runat="server" Text="Hapus" />
        <asp:Button ID="btn_tutup" runat="server" Text="Tutup" />
    </p>
    <p>
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
    </p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            PageSize="5" Width="866px" BackColor="White" BorderColor="#CC9966" 
            BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" />
                <asp:BoundField DataField="id_pelanggan" HeaderText="ID PELANGGAN" />
                <asp:BoundField DataField="nama" HeaderText="NAMA PELANGGAN" />
                <asp:BoundField DataField="alamat" HeaderText="ALAMAT" />
                <asp:BoundField DataField="tgl_lahir" HeaderText="TANGGAL LAHIR" DataFormatString = {0:d} />
                <asp:BoundField DataField="jenis_kelamin" HeaderText="JENIS KELAMIN" />
                <asp:BoundField DataField="no_telp" HeaderText="NO. TELP" />
                <asp:BoundField DataField="email" HeaderText="E-MAIL" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
<p>
        &nbsp;</p>
    </asp:Content>
