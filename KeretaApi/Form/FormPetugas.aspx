﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FormPetugas.aspx.vb" Inherits="KeretaApi.FormPetugas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style3
    {
        width: 63%;
        height: 121px;
    }
    .style4
    {
        width: 148px;
    }
    .style6
    {
        width: 212px;
    }
    .style7
    {
        width: 17px;
    }
    .style8
    {
        width: 212px;
        text-align: center;
    }
    .style9
    {
        width: 230px;
        font-size: x-large;
        text-align: center;
        font-family: "Monotype Corsiva";
        color: #000000;
    }
    .style10
    {
        width: 17px;
        text-align: center;
        font-family: "Monotype Corsiva";
        font-size: x-large;
    }
    .style11
    {
        width: 148px;
        text-align: center;
    }
        .style12
        {
            width: 230px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style3">
    <tr>
        <td class="style11">
            &nbsp;</td>
        <td class="style10">
            &nbsp;</td>
        <td class="style9">
            <strong>DATA PETUGAS</strong></td>
        <td class="style8">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style4">
            ID Petugas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td class="style7">
            :</td>
        <td class="style12">
            <asp:TextBox ID="txt_id" runat="server" Width="210px"></asp:TextBox>
        </td>
        <td class="style6">
        <asp:TextBox ID="txt_state" runat="server" Width="205px"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style4">
            Nama Petugas&nbsp;
        </td>
        <td class="style7">
            :</td>
        <td class="style12">
        <asp:TextBox ID="txt_nama" runat="server" Width="207px"></asp:TextBox>
        </td>
        <td class="style6">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style4">
            Tanggal Lahir&nbsp;&nbsp;&nbsp;
        </td>
        <td class="style7">
            :</td>
        <td class="style12">
        <asp:TextBox ID="txt_tgl" runat="server" Width="207px"></asp:TextBox>
        </td>
        <td class="style6">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style4">
            Jenis Kelamin&nbsp;&nbsp;
        </td>
        <td class="style7">
            :</td>
        <td class="style12">
        <asp:DropDownList ID="ddl_jk" runat="server" Height="18px" Width="208px">
            <asp:ListItem>Laki-Laki</asp:ListItem>
            <asp:ListItem>Perempuan</asp:ListItem>
        </asp:DropDownList>
        </td>
        <td class="style6">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style4">
            No. Telepon&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td class="style7">
            :</td>
        <td class="style12">
        <asp:TextBox ID="txt_telp" runat="server" Width="206px"></asp:TextBox>
        </td>
        <td class="style6">
            &nbsp;</td>
    </tr>
</table>
    <p>
        <asp:Button ID="btn_baru" runat="server" Text="Baru" style="height: 26px" />
        <asp:Button ID="btn_simpan" runat="server" Text="Simpan" />
        <asp:Button ID="btn_batal" runat="server" Text="Batal" />
        <asp:Button ID="btn_hapus" runat="server" Text="Hapus" />
        <asp:Button ID="btn_tutup" runat="server" Text="Tutup" />
    </p>
    <p>
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
    </p>
    <p>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            PageSize="5" Width="590px" BackColor="White" BorderColor="#CC9966" 
            BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" />
                <asp:BoundField DataField="id_petugas" HeaderText="ID PETUGAS" />
                <asp:BoundField DataField="nama" HeaderText="NAMA PETUGAS" />
                <asp:BoundField DataField="tgl_lahir" HeaderText="TANGGAL LAHIR" DataFormatString = {0:d} />
                <asp:BoundField DataField="jenis_kelamin" HeaderText="JENIS KELAMIN" />
                <asp:BoundField DataField="no_telp" HeaderText="NO. TELEPON" />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
</asp:Content>
