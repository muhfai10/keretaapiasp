﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FormTiket.aspx.vb" Inherits="KeretaApi.FormTiket" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    .style1
    {
        width: 74%;
        height: 201px;
    }
    .style4
    {
        height: 21px;
        width: 599px;
    }
    .style5
    {
        width: 24px;
    }
    .style6
    {
        height: 21px;
        width: 24px;
    }
    .style7
    {
        width: 153px;
    }
    .style8
    {
        height: 21px;
        width: 153px;
    }
    .style9
    {
        width: 599px;
    }
    .style10
    {
        font-size: x-large;
    }
    .style11
    {
        width: 153px;
        font-family: "Monotype Corsiva";
        color: #000000;
    }
        .style12
        {
            width: 24px;
            font-size: x-large;
        }
        .style13
        {
            width: 599px;
            color: #000000;
        }
        .style14
        {
            color: #000000;
        }
        .style15
        {
            width: 185px;
        }
        .style16
        {
            height: 21px;
            width: 185px;
        }
        .style17
        {
            width: 185px;
            height: 26px;
        }
        .style18
        {
            width: 24px;
            height: 26px;
        }
        .style19
        {
            width: 153px;
            height: 26px;
        }
        .style20
        {
            width: 599px;
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="style1">
    <tr class="style10">
        <td class="style15">
            <span class="style14"></td>
        <td class="style12">
            </span>
        </td>
        <td class="style11">
            <strong>DATA TIKET</strong></span></td>
        <td class="style13">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style15">
            ID Tiket</td>
        <td class="style5">
            :</td>
        <td class="style7">
            <asp:TextBox ID="txt_idt" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style9">
        <asp:TextBox ID="txt_state" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style15">
            ID Pelanggan</td>
        <td class="style5">
            :</td>
        <td class="style7">
    <asp:DropDownList ID="ddl_pel" runat="server" Height="17px" Width="147px">
    </asp:DropDownList>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style16">
            ID Gerbong</td>
        <td class="style6">
            :</td>
        <td class="style8">
        <asp:DropDownList ID="ddl_gerbong" runat="server" Height="20px" Width="146px">
    </asp:DropDownList>
        </td>
        <td class="style4">
        </td>
    </tr>
    <tr>
        <td class="style15">
            ID Jadwal</td>
        <td class="style5">
            :</td>
        <td class="style7">
        <asp:DropDownList ID="ddl_jadwal" runat="server" Height="16px" Width="147px">
    </asp:DropDownList>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style15">
            Jumlah Tiket</td>
        <td class="style5">
            :</td>
        <td class="style7">
        <asp:TextBox ID="txt_jumlah" runat="server" Width="145px"></asp:TextBox>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style15">
            Jenis Kelas</td>
        <td class="style5">
            :</td>
        <td class="style7">
        <asp:DropDownList ID="ddl_kelas" runat="server" Width="149px">
            <asp:ListItem Value="Ekonomi"></asp:ListItem>
            <asp:ListItem Value="Bisnis"></asp:ListItem>
            <asp:ListItem Value="Eksekutif"></asp:ListItem>
        </asp:DropDownList>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style15">
            Harga Tiket</td>
        <td class="style5">
            :</td>
        <td class="style7">
    <asp:TextBox ID="txt_harga" runat="server" Width="145px"></asp:TextBox>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style17">
            Tanggal Pembelian</td>
        <td class="style18">
            :</td>
        <td class="style19">
        <asp:TextBox ID="txt_tglbeli" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style20">
            </td>
    </tr>
    <tr>
        <td class="style16">
            Kota Tujuan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td class="style6">
            :</td>
        <td class="style8">
            <asp:TextBox ID="txt_kotatujuan" runat="server" Width="146px"></asp:TextBox>
        </td>
        <td class="style4">
            </td>
    </tr>
    <tr>
        <td class="style15">
            Total Harga</td>
        <td class="style5">
            :</td>
        <td class="style7">
        <asp:TextBox ID="txt_total" runat="server" Width="149px"></asp:TextBox>
        </td>
        <td class="style9">
            &nbsp;</td>
    </tr>
</table>
<p>
        <asp:Button ID="btn_baru" runat="server" Text="Baru" Width="61px" />
        <asp:Button ID="btn_simpan" runat="server" Text="Simpan" />
        <asp:Button ID="btn_batal" runat="server" Text="Batal" />
        <asp:Button ID="btn_hapus" runat="server" Text="Hapus" />
        <asp:Button ID="btn_tutup" runat="server" Text="Tutup" />
    </p>
        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            PageSize="5" Width="991px" BackColor="White" BorderColor="#CC9966" 
            BorderStyle="None" BorderWidth="1px">
            <Columns>
                <asp:CommandField SelectText="&gt;&gt;" ShowSelectButton="True" />
                <asp:BoundField DataField="id_tiket" HeaderText="ID TIKET" />
                <asp:BoundField DataField="id_pelanggan" HeaderText="ID PELANGGAN" />
                <asp:BoundField DataField="id_gerbong" HeaderText="ID GERBONG" />
                <asp:BoundField DataField="id_jadwal" HeaderText="ID JADWAL" />
                <asp:BoundField DataField="jumlah_tiket" HeaderText="JUMLAH TIKET" />
                <asp:BoundField DataField="jenis_kelas" HeaderText="JENIS KELAS" />
                <asp:BoundField DataField="harga" HeaderText="HARGA" DataFormatString = {0:C} />
                <asp:BoundField DataField="tanggal_beli" HeaderText="TANGGAL PEMBELIAN" DataFormatString = {0:d} />
                <asp:BoundField DataField="kota_tujuan" HeaderText="KOTA TUJUAN" />
                <asp:BoundField DataField="total_harga" HeaderText="TOTAL HARGA" DataFormatString = {0:C} />
            </Columns>
            <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
            <RowStyle BackColor="White" ForeColor="#330099" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
            <SortedAscendingCellStyle BackColor="#FEFCEB" />
            <SortedAscendingHeaderStyle BackColor="#AF0101" />
            <SortedDescendingCellStyle BackColor="#F6F0C0" />
            <SortedDescendingHeaderStyle BackColor="#7E0000" />
        </asp:GridView>
    </asp:Content>
