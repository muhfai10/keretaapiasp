﻿Public Class FormTiket
    Inherits System.Web.UI.Page
    Private _sql_scalar As String

    Private Property sql_scalar(ByVal p1 As Object, ByVal p2 As String, ByVal p3 As Boolean) As String
        Get
            Return _sql_scalar
        End Get
        Set(ByVal value As String)
            _sql_scalar = value
        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            load_gridview()
            object_state(True)
            button_state(0)
        End If
    End Sub
    Sub object_clear()
        txt_state.Text = ""
        txt_idt.Text = ""
        txt_jumlah.Text = ""
        txt_tglbeli.Text = ""
        txt_harga.Text = ""
        txt_total.Text = ""
        txt_kotatujuan.Text = ""
        Label1.Text = ""
    End Sub
    Sub object_fill(ByVal row As GridViewRow)
        ddl_pel.Items.Clear()
        ddl_gerbong.Items.Clear()
        ddl_jadwal.Items.Clear()
        txt_idt.Text = row.Cells(1).Text
        ddl_pel.Items.Add(row.Cells(2).Text)
        ddl_gerbong.Items.Add(row.Cells(3).Text)
        ddl_jadwal.Items.Add(row.Cells(4).Text)
        txt_jumlah.Text = row.Cells(5).Text
        ddl_kelas.Text = row.Cells(6).Text
        txt_harga.Text = row.Cells(7).Text
        txt_tglbeli.Text = row.Cells(8).Text
        txt_kotatujuan.Text = row.Cells(9).Text
        txt_total.Text = row.Cells(10).Text

    End Sub
    Sub button_state(ByVal id As Integer)
        Select Case id
            Case 0 'default
                btn_baru.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = False
                btn_hapus.Enabled = False
                btn_simpan.Enabled = False
            Case 1 'new
                btn_simpan.Enabled = True
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_baru.Enabled = False
                btn_hapus.Enabled = False
            Case 2 'edit
                btn_baru.Enabled = False
                btn_tutup.Enabled = True
                btn_batal.Enabled = True
                btn_hapus.Enabled = True
                btn_simpan.Enabled = True
        End Select

    End Sub
    Sub object_state(ByVal kunci As Boolean)
        txt_idt.ReadOnly = Not kunci
        ddl_pel.Enabled = True
        ddl_gerbong.Enabled = True
        ddl_jadwal.Enabled = True
        txt_jumlah.ReadOnly = Not kunci
        ddl_kelas.Enabled = True
        txt_harga.ReadOnly = Not kunci
        txt_tglbeli.ReadOnly = Not kunci
        txt_kotatujuan.ReadOnly = Not kunci
        txt_total.ReadOnly = Not kunci

    End Sub
    Protected Sub btn_baru_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_baru.Click
        Try
            If sql_conn(Session("currCon")) = True Then
                Dim sql1 As String = "select id_pelanggan from Pelanggan"
                Dim sql2 As String = "select id_gerbong from Gerbong"
                Dim sql3 As String = "select id_jadwal from Jadwal"
                Dim RS1 As OleDb.OleDbDataReader
                Dim RS2 As OleDb.OleDbDataReader
                Dim RS3 As OleDb.OleDbDataReader
                RS1 = sql_datareader(Session("currCon"), sql1)
                RS2 = sql_datareader(Session("currCon"), sql2)
                RS3 = sql_datareader(Session("currCon"), sql3)
                ddl_pel.Items.Clear()
                ddl_gerbong.Items.Clear()
                ddl_jadwal.Items.Clear()
                If RS1.HasRows Then
                    While RS1.Read
                        ddl_pel.Items.Add(RS1("id_pelanggan"))
                    End While
                End If
                If RS2.HasRows Then
                    While RS2.Read
                        ddl_gerbong.Items.Add(RS2("id_gerbong"))
                    End While
                End If
                If RS3.HasRows Then
                    While RS3.Read
                        ddl_jadwal.Items.Add(RS3("id_jadwal"))
                    End While
                End If
            End If
            object_clear()
            object_state(True)
            button_state(1)
            txt_state.Text = "new"
            Page.SetFocus(txt_idt)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btn_simpan_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_simpan.Click
        Try
            If sql_conn(Session("currCon")) = True Then
                Dim sql As String = ""
                If txt_state.Text = "new" Then
                    sql = "insert into Tiket values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')"
                    sql = String.Format(sql, txt_idt.Text, ddl_pel.Text, ddl_gerbong.Text, ddl_jadwal.Text, txt_jumlah.Text, ddl_kelas.Text, txt_harga.Text, txt_tglbeli.Text, txt_kotatujuan.Text, txt_total.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_idt.Text = "id_tiket" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Disimpan"
                    End If


                ElseIf txt_state.Text = "edit" Then
                    sql = "update Tiket set id_tiket='{0}', id_pelanggan='{1}', id_gerbong='{2}', id_jadwal='{3}', jumlah_tiket='{4}', jenis_kelas='{5}', harga='{6}', tanggal_beli='{7}',kota_tujuan='{8}', total_harga='{9}' where id_tiket='{0}'"
                    sql = String.Format(sql, txt_idt.Text, ddl_pel.Text, ddl_gerbong.Text, ddl_jadwal.Text, txt_jumlah.Text, ddl_kelas.Text, txt_harga.Text, txt_tglbeli.Text, txt_kotatujuan.Text, txt_total.Text, txt_idt.Text)
                    sql_execute(Session("currCon"), sql)
                    Label1.Visible = True
                    If txt_idt.Text = "id_tiket" Then
                        Label1.Text = "Data Telah ada !!!"
                    Else
                        Label1.Text = "Data berhasil Diubah"
                    End If
                End If
            End If
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
        Catch ex As Exception
            MsgBox(Err.Description, vbOKOnly, "Error")
        End Try
    End Sub
    Sub load_gridview()
        If sql_conn(Session("currCon")) = True Then
            Dim tbl_grid As New DataTable
            tbl_grid = sql_datatable(Session("currCon"), "SELECT * FROM Tiket ORDER BY id_tiket")
            GridView1.DataSource = tbl_grid
            GridView1.DataBind()
            sql_conn_close(Session("currCon"))
        End If
    End Sub
    Private Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        load_gridview()
        GridView1.PageIndex = e.NewPageIndex
        GridView1.DataBind()
    End Sub

    Private Sub GridView1_SelectedIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSelectEventArgs) Handles GridView1.SelectedIndexChanging
        Dim row_edit As GridViewRow = GridView1.Rows(e.NewSelectedIndex)
        object_clear()
        object_state(True)
        button_state(2)
        object_fill(row_edit)
        txt_state.Text = "edit"
    End Sub

    Protected Sub btn_batal_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_batal.Click
        object_clear()
        object_state(True)
        button_state(0)
    End Sub

    Protected Sub btn_hapus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_hapus.Click
        If sql_conn(Session("currCon")) = True Then
            sql_execute(Session("currCon"), "DELETE FROM Tiket WHERE id_tiket='" & txt_idt.Text & "'")
            sql_conn_close(Session("currCon"))
            load_gridview()
            object_clear()
            object_state(True)
            button_state(0)
            Label1.Text = "Data berhasil dihapus"
        End If
    End Sub

    Protected Sub btn_tutup_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_tutup.Click
        sql_conn_close(Session("currCon"))
        Response.Redirect("~/default.aspx")
    End Sub



    Protected Sub txt_jumlah_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txt_jumlah.TextChanged
        If ddl_kelas.Text = "Ekonomi" Then
            txt_harga.Text = 2500
        ElseIf ddl_kelas.Text = "Bisnis" Then
            txt_harga.Text = 5000
        Else
            txt_harga.Text = 10000
        End If
    End Sub

    Protected Sub txt_harga_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txt_harga.TextChanged
        txt_total.Text = Val(txt_harga.Text) * Val(txt_jumlah.Text)
    End Sub
End Class